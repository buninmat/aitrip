# AI Trip

Simple photo editor using DeepAi APIs

## Application should use the following APIs:
- Google Photos
- DeepAI APIs
    - https://deepai.org/machine-learning-model/deepdream
    - https://deepai.org/machine-learning-model/fast-style-transfer
    - ...
- Own API
    - Save user workflow and edited pictures

## Features
- Upload photo/select from Google Photos
- Search for photos
- Applying deepapi filter
- Download photo/Copy/save to Google Photos